#use wml::debian::template title="Debian 11 -- Fejl" BARETITLE=true
#use wml::debian::toc
#use wml::debian::translation-check translation="1b53acd1a1236a10ef76b706c36cf6495d8ebfcf"

#include "$(ENGLISHDIR)/releases/info"

<toc-display/>

# <toc-add-entry name="known_probs">Kendte problemer</toc-add-entry>
<toc-add-entry name="security">Sikkerhedsproblemer</toc-add-entry>

<p>Debian Security-holdet udsender opdateringer til pakker i den stabile udgave, 
hvor der er registreret sikkerhedsrelaterede problemer.  Besøg 
<a href="$(HOME)/security/">sikkerhedssiderne</a> for oplysninger om alle 
sikkerhedsproblemer registreret i <q>bullseye</q>.</p>

<p>Hvis du anvender APT, kan følende linje føjes til 
<tt>/etc/apt/sources.list</tt>, for at kunne tilgå de seneste 
sikkerhedsopdateringer:</p>

<pre>
  deb http://security.debian.org/ bullseye-security main contrib non-free
</pre>

<p>Derefter køres <kbd>apt update</kbd> efterfulgt af 
<kbd>apt upgrade</kbd>.</p>


<toc-add-entry name="pointrelease">Punktudgivelser</toc-add-entry>

<p>Nogle gange, i tilfælde af kritiske problemer eller sikkerhedsopdateringer, 
opdateres den udgivne distribution.  Generelt kaldes disse for punktudgivelser)
(eller <q>point releases</q> på engelsk).</p>

<!-- 
<ul>
  <li>Den første punktopdatering, 11.1, blev udgivet den
      <a href="$(HOME)/News/2017/FIXME">FIXME</a>.</li>
</ul> 
-->

<ifeq <current_release_bullseye> 11.0 "

<p>Der er endnu ingen punktudgivelser til Debian 11.</p>" "

<p>Se 
<a href="http://http.us.debian.org/debian/dists/bullseye/ChangeLog">\
ChangeLog</a> for detaljerede ændringer mellem 11.0 og 
<current_release_bullseye/>.</p>"/>

<p>Rettelser til den udgivne stabile distribution gennemgår ofte en udvidet 
testperiode, før de accepteret i arkivet.  Dog er disse ændringer tilgængelige 
i mappen 
<a href="http://ftp.debian.org/debian/dists/bullseye-proposed-updates/">\
dists/bullseye-proposed-updates</a> på alle Debians arkivfilspejle.</p>

<p>Hvis du anvender APT til at opdatere dine pakker, kan du installere de 
foreslåede opdateringer, ved at føje følgende linje til 
<tt>/etc/apt/sources.list</tt>:</p>

<pre>
  \# proposed additions for a 11 point release
  deb http://deb.debian.org/debian bullseye-proposed-updates main contrib non-free
</pre>

<p>Derefter køres <kbd>apt update</kbd> efterfulgt af 
<kbd>apt upgrade</kbd>.</p>


<toc-add-entry name="installer">Installeringssystem</toc-add-entry>

<p>For oplysninger om fejl og opdateringer til installeringssystemet, se 
siden med <a href="debian-installer/">installeringsoplysninger</a>.</p>
