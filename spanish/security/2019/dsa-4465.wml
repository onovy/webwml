#use wml::debian::translation-check translation="8a5eb3970afae92528c267b43ff2cff70683b130"
<define-tag description>actualización de seguridad</define-tag>
<define-tag moreinfo>
<p>Se han descubierto varias vulnerabilidades en el núcleo Linux que
pueden dar lugar a elevación de privilegios, a denegación de servicio o a fugas
de información.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-3846">CVE-2019-3846</a>,
	<a href="https://security-tracker.debian.org/tracker/CVE-2019-10126">CVE-2019-10126</a>

    <p>huangwen informó de múltiples desbordamientos de memoria en el controlador wifi
    Marvell (mwifiex) que un usuario local podría usar para provocar denegación de
    servicio o ejecución de código arbitrario.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-5489">CVE-2019-5489</a>

    <p>Daniel Gruss, Erik Kraft, Trishita Tiwari, Michael Schwarz, Ari
    Trachtenberg, Jason Hennessey, Alex Ionescu y Anders Fogh
    descubrieron que los usuarios locales podrían utilizar la llamada al sistema mincore() para
    obtener información sensible de otros procesos que accedieran al
    mismo fichero proyectado en memoria.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-9500">CVE-2019-9500</a>,
	<a href="https://security-tracker.debian.org/tracker/CVE-2019-9503">CVE-2019-9503</a>

    <p>Hugues Anguelkov descubrió un desbordamiento de memoria y ausencia de validación
    de acceso en el controlador wifi Broadcom FullMAC (brcmfmac) que un
    atacante en la misma red wifi podría usar para provocar denegación de
    servicio o ejecución de código arbitrario.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-11477">CVE-2019-11477</a>

    <p>Jonathan Looney informó de que una secuencia de acuses de recibo selectivos (SACKs) TCP
    preparada de una manera determinada permite desencadenar de forma remota un
    kernel panic.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-11478">CVE-2019-11478</a>

    <p>Jonathan Looney informó de que una secuencia de acuses de recibo selectivos (SACKs) TCP
    preparada de una manera determinada fragmenta la cola
    de retransmisiones TCP, permitiendo que un atacante provoque un consumo
    de recursos excesivo.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-11479">CVE-2019-11479</a>

    <p>Jonathan Looney informó de que un atacante podría obligar al núcleo
    Linux a fragmentar sus respuestas en múltiples segmentos TCP, cada uno
    de los cuales contendría solo ocho bytes de datos, aumentando drásticamente el
    ancho de banda necesario para transmitir una cantidad de datos dada.</p>

    <p>Esta actualización incorpora una nueva variable de sysctl para controlar el MSS mínimo
    (net.ipv4.tcp_min_snd_mss) que, por omisión, toma el valor 48, que antes estaba fijado en el código. Recomendamos elevar este valor a 536 salvo que sepa
    que su red necesita un valor menor.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-11486">CVE-2019-11486</a>

    <p>Jann Horn, de Google, informó de numerosas condiciones de carrera en la
    capa «line discipline» del Siemens R3964. Un usuario local podría usar estas para
    provocar impactos de seguridad no especificados. En consecuencia, se ha inhabilitado este
    módulo.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-11599">CVE-2019-11599</a>

    <p>Jann Horn, de Google, informó de una condición de carrera en la implementación del
    volcado de memoria («core dump») que podría dar lugar a un «uso tras liberar». Un usuario
    local podría usar esto para leer información sensible, para provocar
    denegación de servicio (corrupción de memoria) o para elevar
    privilegios.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-11815">CVE-2019-11815</a>

    <p>Se descubrió que un «uso tras liberar» en el protocolo de «conexiones fiables de datagramas» («Reliable
    Datagram Sockets») podría dar lugar a denegación de servicio y, potencialmente,
    a elevación de privilegios. Este módulo de protocolo (rds) no se carga por omisión en los sistemas Debian, por lo que este problema afecta solamente a sistemas en los que
    el módulo se carga explícitamente.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-11833">CVE-2019-11833</a>

    <p>Se descubrió que la implementación del sistema de ficheros ext4 escribe
    datos sin inicializar procedentes de la memoria del núcleo en bloques de extensiones nuevas. Un
    usuario local con capacidad para escribir en un sistema de ficheros ext4 y, a continuación, leer la
    imagen del sistema de archivos, por ejemplo utilizando un dispositivo extraíble,
    podría usar esto para hacerse con información sensible.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-11884">CVE-2019-11884</a>

    <p>Se descubrió que la implementación de Bluetooth HIDP no
    garantizaba que los nombres de las conexiones nuevas terminaran con el carácter nulo. Un usuario
    local con capacidad CAP_NET_ADMIN podría usar esto para
    hacerse con información sensible procedente de la pila del núcleo.</p></li>

</ul>

<p>Para la distribución «estable» (stretch), estos problemas se han corregido en
la versión 4.9.168-1+deb9u3.</p>

<p>Le recomendamos que actualice los paquetes de linux.</p>

<p>Para información detallada sobre el estado de seguridad de linux, consulte su página
en el sistema de seguimiento de problemas de seguridad:
<a href="https://security-tracker.debian.org/tracker/linux">https://security-tracker.debian.org/tracker/linux</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2019/dsa-4465.data"
