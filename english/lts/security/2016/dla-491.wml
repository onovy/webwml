<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>The PostgreSQL project released a new version of the PostgreSQL 9.1 branch:</p>

<ul>

<li>Clear the OpenSSL error queue before OpenSSL calls, rather than assuming it's
clear already; and make sure we leave it clear afterwards (Peter Geoghegan,
Dave Vitek, Peter Eisentraut)</li>

</ul>

<p>This change prevents problems when there are multiple connections using OpenSSL
within a single process and not all the code involved follows the same rules
for when to clear the error queue. Failures have been reported specifically
when a client application uses SSL connections in libpq concurrently with SSL
connections using the PHP, Python, or Ruby wrappers for OpenSSL. It's possible
for similar problems to arise within the server as well, if an extension module
establishes an outgoing SSL connection.</p>

<ul>

<li>Fix "failed to build any N-way joins" planner error with a full join enclosed
in the right-hand side of a left join (Tom Lane)</li>

<li>Fix possible misbehavior of TH, th, and Y,YYY format codes in to_timestamp()
(Tom Lane)</li>

</ul>

<p>These could advance off the end of the input string, causing subsequent format
codes to read garbage.</p>

<ul>

<li>Fix dumping of rules and views in which the array argument of a value
operator ANY (array) construct is a sub-SELECT (Tom Lane)</li>

<li>Make pg_regress use a startup timeout from the PGCTLTIMEOUT environment
variable, if that's set (Tom Lane)</li>

</ul>

<p>This is for consistency with a behavior recently added to pg_ctl; it eases
automated testing on slow machines.</p>

<ul>

<li>Fix pg_upgrade to correctly restore extension membership for operator
families containing only one operator class (Tom Lane)</li>

</ul>

<p>In such a case, the operator family was restored into the new database, but it
was no longer marked as part of the extension. This had no immediate ill
effects, but would cause later pg_dump runs to emit output that would cause
(harmless) errors on restore.</p>

<ul>

<li>Rename internal function strtoi() to strtoint() to avoid conflict with a
NetBSD library function (Thomas Munro)</li>

<li>Use the FORMAT_MESSAGE_IGNORE_INSERTS flag where appropriate. No live bug is
known to exist here, but it seems like a good idea to be careful.</li>

</ul>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
9.1.22-0+deb7u1.</p>

<p>We recommend that you upgrade your postgresql-9.1 packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-491.data"
# $Id: $
