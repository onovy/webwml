<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>tzdata upstream released version 2018g.</p>

<p>Notables changes since 2018e (previous version available in jessie)
include:

<ul>
<li>Morocco switched to permanent +01 on 2018-10-27.</li>
<li>Volgograd moved from +03 to +04 on 2018-10-28.</li>
<li>Fiji ends DST 2019-01-13, not 2019-01-20.</li>
<li>Most of Chile changes DST dates, effective 2019-04-06.</li>
</ul>

<p>For Debian 8 <q>Jessie</q>, this problem has been fixed in version
2018g-0+deb8u1.</p>

<p>We recommend that you upgrade your tzdata packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1563.data"
# $Id: $
