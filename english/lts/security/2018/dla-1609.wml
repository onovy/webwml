<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>A vulnerability has been discovered in libapache-mod-jk, the Apache 2
connector for the Tomcat Java servlet engine.</p>

<p>The libapache-mod-jk connector is susceptible to information disclosure
and privilege escalation because of a mishandling of URL normalization.</p>

<p>The nature of the fix required that libapache-mod-jk in Debian 8
<q>Jessie</q> be updated to the latest upstream release.  For reference, the
upstream changes associated with each release version are documented
here:</p>

<p><url "http://tomcat.apache.org/connectors-doc/miscellaneous/changelog.html"></p>

<p>For Debian 8 <q>Jessie</q>, this problem has been fixed in version
1.2.46-0+deb8u1.</p>

<p>We recommend that you upgrade your libapache-mod-jk packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1609.data"
# $Id: $
