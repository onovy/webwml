<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>A second regression issue has been resolved in the poppler PDF rendering
shared library this time introduced with version 0.26.5-2+deb8u6 (see
DLA 1562-2).</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-16646">CVE-2018-16646</a>

    <p>In Poppler 0.68.0, the Parser::getObj() function in Parser.cc may
    cause infinite recursion via a crafted file. A remote attacker can
    leverage this for a DoS attack.</p>

    <p>The poppler upstream developers added two more regression fixes on top
    of the original fix (upstream merge request #91). These two patches
    have now been added to the poppler package in Debian jessie LTS.</p></li>

</ul>

<p>For Debian 8 <q>Jessie</q>, this problem has been fixed in version
0.26.5-2+deb8u7.</p>

<p>We recommend that you upgrade your poppler packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1562-3.data"
# $Id: $
