<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Several issues have been discovered in PHP (recursive acronym for PHP:
Hypertext Preprocessor), a widely-used open source general-purpose
scripting language that is especially suited for web development and can
be embedded into HTML.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-10397">CVE-2016-10397</a>

  <p>Incorrect handling of various URI components in the URL parser could
  be used by attackers to bypass hostname-specific URL checks.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-11143">CVE-2017-11143</a>

  <p>An invalid free in the WDDX deserialization of boolean parameters
  could be used by attackers able to inject XML for deserialization to
  crash the PHP interpreter.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-11144">CVE-2017-11144</a>

  <p>The openssl extension PEM sealing code did not check the return value
  of the OpenSSL sealing function, which could lead to a crash of the
  PHP interpreter.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-11145">CVE-2017-11145</a>

  <p>Lack of a bounds check in the date extension's timelib_meridian
  parsing code could be used by attackers able to supply date strings to
  leak information from the interpreter.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-11147">CVE-2017-11147</a>

  <p>The PHAR archive handler could be used by attackers supplying
  malicious archive files to crash the PHP interpreter or potentially
  disclose information due to a buffer over-read in the
  phar_parse_pharfile function in ext/phar/phar.c.</p></li>

</ul>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
5.4.45-0+deb7u9.</p>

<p>We recommend that you upgrade your php5 packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-1034.data"
# $Id: $
