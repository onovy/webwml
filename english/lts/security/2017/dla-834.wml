<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>A server-side request forgery vulnerability was reported for the setup
script in phpmyadmin, a MYSQL web administration tool. This flaw may
allow an unauthenticated attacker to brute-force MYSQL passwords,
detect internal hostnames or opened ports on the internal network.
Additionally there was a race condition between writing configuration
and administrator moving it allowing unauthenticated users to read or
alter it. Debian users who configured phpmyadmin via debconf and used
the default configuration for Apache 2 or Lighttpd were never affected.</p>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
4:3.4.11.1-2+deb7u8.</p>

<p>We recommend that you upgrade your phpmyadmin packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-834.data"
# $Id: $
