#use wml::debian::template title="Programme de Joerg Jaspert" BARETITLE="true" NOHEADER="true"
#include "$(ENGLISHDIR)/vote/style.inc"
#use wml::debian::translation-check translation="fba0207f2c0869153f2b302039417c51e80eaf2a" maintainer="Jean-Pierre Giraud"

<h1 class="title">Programme de DPL 2019</h1>
<div id="table-of-contents">
<h2>Table des matières</h2>
<div id="text-table-of-contents">
<ul>
<li><a href="#orgfb93861">1. Qui suis-je ?</a></li>
<li><a href="#org781b5a7">2. Activités Debian</a></li>
<li><a href="#orga50bc7e">3. Le chef du projet Debian en 2019</a></li>
<li><a href="#org5f677db">4. L'avenir</a></li>
<li><a href="#org230864e">5. Mon rôle en tant que DPL</a></li>
<li><a href="#orgabc7e56">6. Engagement en termes de temps</a></li>
<li><a href="#orgb0cb51b">7. Et à propos des délégations ?</a></li>
<li><a href="#org2ed253a">8. Réfutations</a>
<ul>
<li><a href="#org86c63df">8.1. Jonathan Carter</a></li>
<li><a href="#orgcfb0089">8.2. Sam Hartman</a></li>
<li><a href="#orgb90ee39">8.3. Martin Michlmayer</a></li>
<li><a href="#org120099b">8.4. Simon Richter</a></li>
<li><a href="#orgcb30e0c">8.5. Commentaire général</a></li>
</ul>
</li></ul>
</div>
</div>

<div id="outline-container-orgfb93861" class="outline-2">
<h2 id="orgfb93861"><span class="section-number-2">1</span> Qui suis-je ?</h2>
<div class="outline-text-2" id="text-1">
<p>
Je suis développeur Debian depuis 17 ans, ayant rejoint Debian en avril 2012.
Les gens dans la vie réelle pensent que je suis « LA RÉPONSE » depuis
mi-mars, car j'ai 42 ans depuis peu. Je vis dans une charmante ville
moyenne appelée Fulda, en plein centre de l'Allemagne.
</p>
</div>
</div>

<div id="outline-container-org781b5a7" class="outline-2">
<h2 id="org781b5a7"><span class="section-number-2">2</span> Activités Debian</h2>
<div class="outline-text-2" id="text-2">
<p>
J'ai tenu divers stands pour Debian dans un certain nombre de manifestations
au fil des ans. Cela a été le cas pendant plusieurs LinuxTag,
LinuxWorldExpo et Cebit, ainsi que lors de Journées Linux de Chemitz, du
FrosCon et d'autres manifestations plus modestes. Parfois je ne faisais
qu'être présent, mais dans d'autres cas, j'ai géré toute l'organisation
nécessaire, y compris la production de CD ou DVD Debian à distribuer aux
visiteurs aussi bien que la présentation de produits. C'est durant la
première de ces manifestations que j'ai trouvé mon premier paquet à
entretenir (ou plutôt c'est le développeur amont qui m'a trouvé).
</p>

<p>
Je suis devenu responsable de candidature dans le processus du nouveau
membre peu de temps après être devenu DD puis gestionnaire adjoint de
compte Debian en 2004 avant de devenir gestionnaire de compte à part
entière (DAM) en avril 2008.
</p>

<p>
En mars 2005, j'ai rejoint l'équipe FTP comme assistant et suis devenu un
responsable de l'archive (FTP Master) en avril 2008.
</p>

<p>
Après la DebConf5, j'ai rejoint l'organisation des DebConf avec comme rôle
principal celui de l'administration de l'infrastructure, à l'époque où elle
était distincte de Debian. Au-delà du travail d'administration, j'ai aidé
ou piloté plusieurs équipes comme celle en charge du programme ou du
soutien financier aux déplacements.
</p>

<p>
Je fais partie du groupe d'administration des miroirs, et j'ai écrit la base
de l'ensemble de scripts de synchronisation des miroirs avec le site principal.
</p>

<p>
Je suis un des administrateurs de notre Debian Planet ainsi que du service
gitlab de Debian salsa.debian.org.
</p>

<p>
Je suis aussi membre de l'équipe (NOC - Centre d’opération réseau) d'OFTC,
du réseau IRC vers lequel pointe irc.debian.org.
</p>

<p>
Entre 2007 et février 2018, j'ai été membre du bureau de SPI, (« Software
in the Public Interest », logiciel d'intérêt public), la plupart du temps
comme vice-président et administrateur principal. SPI est la couverture
légale dans laquelle Debian réside aux États-Unis, et qui détient la
majorité des avoirs de Debian.
</p>
</div>
</div>

<div id="outline-container-orga50bc7e" class="outline-2">
<h2 id="orga50bc7e"><span class="section-number-2">3</span> Le chef du projet Debian en 2019</h2>
<div class="outline-text-2" id="text-3">
<p>
C'est à nouveau le moment dans l'année. Il y beaucoup de monde sur la
ligne de départ, prêts à poser une rafale de questions sur la liste
-vote, avec seulement cinq candidats pour répondre. C'est la période
d'élection du DPL et des longues discussions sur la liste. Tout le monde
veut savoir comment le prochain chef du projet va nous mener vers le
futur glorieux du système d'exploitation universel.
</p>

<p>
Comment relever tous les défis qui se posent à une distribution comme la
nôtre, comment s'adapter à un monde qui évolue toujours plus vite ?
</p>

<p>
Comment devenir plus attractif pour toutes les entités commerciales qui
choisissent actuellement d'ignorer Debian mais préfèrent une distribution
dérivée ?
</p>

<p>
Comment devenir plus attractif pour les développeurs ?
</p>

<p>
Comment renouveler nos outils pour un flux de travail plus moderne ? Il
y a un désir partagé de moderniser le BTS (une interface web, quelqu'un ?),
ou pour des envois avec un « git push ».
</p>

<p>
Comment améliorer la diversité ? (Il suffit de regarder la liste des
candidats cette année&#x2026;)
</p>

<p>
Comment simplifier des modifications à grande échelle dans Debian ?
</p>

<p>
Comment traiter la mentalité « curl | sudo bash » des langages les plus
récents et de leur environnement ? Comment inclure utilement ces
langages ?
</p>

<p>
Et j'en suis sûr, beaucoup d'autres questions.
</p>
</div>
</div>

<div id="outline-container-org5f677db" class="outline-2">
<h2 id="org5f677db"><span class="section-number-2">4</span> L'avenir</h2>
<div class="outline-text-2" id="text-4">
<p>
Je ne suis pas très bon pour prédire l'avenir, mais il y une chose sur
laquelle je suis prêt à parier, c'est que Debian continuera à être
pertinent pour la communauté. Nous avons beaucoup d'utilisateurs, directs
ou indirects au travers des dérivées de Debian. Ils continuent à avoir
besoin d'un système stable, prévisible et qui fonctionne tout simplement,
ce que nous sommes doués pour le fournir.
</p>

<p>
Nous ne devrions pas nous relâcher ni nous contenter d'être juste un socle
pour les autres, nous devrions examiner pourquoi les gens
recherchent autre chose que Debian. Et voir si nous pouvons améliorer
Debian pour fournir de nouvelles fonctionnalités, tout en gardant
l'équilibre avec nos utilisateurs actuels. Et oui, cela comprend les
utilisateurs qui construisent leurs distributions sur la nôtre.
</p>

<p>
Nous devons répondre aux besoins de nos millions d'utilisateurs. Leur
éventail est vaste, de ceux qui souhaitent un système vraiment stable et
répugnent au changement à ceux qui trouvent même que notre distribution
unstable est trop lente à évoluer.
</p>

<p>
Ce n'est pas une tâche impossible, nous avons des gens parmi les plus
brillants qui travaillent avec le projet. Nous sommes les experts pour
assurer l'intégration de nos logiciels. Et nous devons nous assurer de
sauvegarder ces propriétés et les étendre aux nouveaux défis.
</p>

<p>
Et certains de ces défis sont énormes. Le battage actuel sur les
« parfaites manières de faire sysadmin/devops/&#x2026; » est rarement basé
sur des distributions complètes.
</p>
</div>
</div>

<div id="outline-container-org230864e" class="outline-2">
<h2 id="org230864e"><span class="section-number-2">5</span> Mon rôle en tant que DPL</h2>
<div class="outline-text-2" id="text-5">
<p>
Dans la mesure ou le DPL n'est pas le leader du développement technique
effectif, ce n'est pas au DPL de trouver des solutions techniques aux défis
auxquels nous faisons face. Ni de trouver des réponses parfaites à la série
de questions que j'ai posées plus haut (et à toutes les autres).
</p>

<p>
Mais le DPL doit permettre à d'autres membres du projet de le faire, et
cela constitue un rôle majeur sur lequel je mettrai l'accent. Beaucoup des
points que j'ai évoqués ne sont pas de ceux qui peuvent être mis en œuvre à
court terme, en un an par exemple, mais on peut au moins commencer à les
traiter. Certains ont déjà commencé à le faire, et il est important de
poursuivre ce travail et d'entretenir l'engagement des gens.
</p>

<p>
Le DPL devrait travailler à supprimer les blocages et permettre aux
développeurs de poursuivre leur travail.
</p>

<p>
C'est aussi le travail du DPL d'être à l'écoute. Et d'être guidé par la
communauté.
</p>

<p>
Debian ne consiste pas seulement à empaqueter et à d'autres tâches
techniques similaires, nous avons beaucoup d'autres contributions aussi
importantes. Nous avons besoin d'encourager plus de gens qui ne connaissent
pas bien le travail technique à contribuer dans divers domaines non
techniques. Cela va de l'écriture de documentations, des traductions, de
la contribution au site web, au travail de conception, à l'assistance aux
utilisateurs ou bien à la représentation de Debian à des manifestations. Ou
à organiser des événements, tels que des chasses aux bogues ou des réunions
de « miniDebconf ». Tout cela est aussi important que le travail des
empaqueteurs.
</p>

<p>
Je travaillerai avec tous ceux qui souhaitent améliorer les processus à
l'intérieur de Debian pour identifier les obstacles importants, pour
réduire notre complexité et pour améliorer la communication entre les
équipes si besoin est.
</p>

<p>
J'ai l'intention de poursuivre la démarche de transparence de Chris (et
d'autres anciens DPL) avec des rapports d'activité réguliers, bien que mon
style sera naturellement différent.
</p>

<p>
Aussi, comme nous sommes dans un projet vraiment énorme, il y a des
secteurs où je n'en sais pas assez (voire rien). Je n'ai pas peur de
demander de l'aide.
</p>
</div>
</div>

<div id="outline-container-orgabc7e56" class="outline-2">
<h2 id="orgabc7e56"><span class="section-number-2">6</span> Engagement en termes de temps</h2>
<div class="outline-text-2" id="text-6">
<p>
Le travail de DPL est un de ceux qui réclament beaucoup de temps
et d'énergie. Avoir un travail à temps complet et une famille avec deux
enfants ne facilite pas les choses. À ce sujet, j'ai discuté avec tous ceux
qui étaient concernés et j'ai obtenu leur soutien plein et entier pour ma
candidature.
</p>

<p>
Pour être plus précis : mon entreprise a compté 45 journées à employer
pour mes fonctions de DPL (sur les environs 250 jours travaillés que compte
un mandat de DPL), en plus de mes jours de congé habituels.
</p>

<p>
Le temps que je prendrai dépendra des tâches réelles du DPL et cela pourra
aller de quelques heures par jour à des absences complètes pour me rendre à
des manifestations.
</p>

<p>
Ce n'est que ce que je prendrai sur mon temps de travail, le temps effectif
dont je pourrai disposer pour mes obligations de DPL sera plus important,
mais je ne vais pas en faire un décompte définitif. Je l'ajusterai aux
besoins, lorsqu'il le faudra.
</p>
</div>
</div>

<div id="outline-container-orgb0cb51b" class="outline-2">
<h2 id="orgb0cb51b"><span class="section-number-2">7</span> Et à propos des délégations ?</h2>
<div class="outline-text-2" id="text-7">
<p>
Commençons par la plus importante dans ce contexte, celle de DAM. La
constitution est claire dans ce cas. Le paragraphe 8.1.2 lui est consacré.
Il précise :
</p>

<blockquote>
<p>
Les délégués du chef du projet :
</p>

<p>
[&#x2026;]
</p>

<p>
Ils peuvent prendre certaines décisions que le DPL ne peut pas prendre
lui-même directement, y compris l'acceptation ou l'expulsion de
développeurs, ou en désignant comme développeur des personnes qui
n'entretiennent pas de paquets. Cela est fait pour éviter une concentration
du pouvoir dans les mains du DPL, en particulier pour l'affiliation des
développeurs.
</p>
</blockquote>
<p>
La conclusion à tirer est simple, si je suis élu, je me retirerai du
rôle de DAM.
</p>

<p>
Au-delà de la citation ci-dessus, la constitution n'interdit que la
candidature du secrétaire et du président du CTTE pour le poste de DPL, et
donc je ne vois pas de conflit qui m'interdise de conserver mon autre
délégation, celle de FTP Master. Autrement dit, quand le moment sera venu
de la renouveler, je devrai l'abandonner dans la mesure où le DPL ne peut
pas s'accorder de délégation.
</p>
</div>
</div>

<div id="outline-container-org2ed253a" class="outline-2">
<h2 id="org2ed253a"><span class="section-number-2">8</span> Réfutations</h2>
<div class="outline-text-2" id="text-8">
</div>
<div id="outline-container-org86c63df" class="outline-3">
<h3 id="org86c63df"><span class="section-number-3">8.1</span> Jonathan Carter</h3>
<div class="outline-text-3" id="text-8-1">
<p>
Je suis d'accord avec lui pour ne pas écarter de grands objectifs qu'on
doive atteindre durant un mandat. Ses objectifs et projets d'exécution me
semblent pour la plupart corrects, bien que je trouve problématique
d'annoncer déjà le temps qu'il ne passera pas à travailler comme DPL.
</p>

<p>
Je ne crois pas que le rôle de DPL devrait être traité comme cela.
</p>
</div>
</div>

<div id="outline-container-orgcfb0089" class="outline-3">
<h3 id="orgcfb0089"><span class="section-number-3">8.2</span> Sam Hartman</h3>
<div class="outline-text-3" id="text-8-2">
<p>
Son objectif principal semble être un nouveau style de communication
gardant à l'esprit les sentiments de chacun, de garder courts les processus
et que Debian reste agréable pour les membres du projet. De ce que je
connais de lui, je suis sûr qu'il sera capable de réaliser beaucoup dans ce
sens. En fait, si je l'emporte, j'aimerai travailler avec lui sur cet
objectif, car c'est un point important et il est meilleur que moi dans ce
domaine.
</p>

<p>
Le reste de son programme me semble de poursuivre le travail quotidien
habituel du DPL.
</p>
</div>
</div>

<div id="outline-container-orgb90ee39" class="outline-3">
<h3 id="orgb90ee39"><span class="section-number-3">8.3</span> Martin Michlmayer</h3>
<div class="outline-text-3" id="text-8-3">
<p>
Il débute son programme en admettant qu'il souhaitait renoncer mais que,
bon, maintenant, allons-y pour une autre période comme DPL. Le programme se
poursuit par une longue série de points listant ce qui lui semble ne pas
aller dans Debian.
</p>

<p>
Un des points les plus discutables, et celui qui sera probablement le plus
discutable s'il est élu, est la partie où il dit que Debian a besoin de plus
de développeurs salariés et de plus d'implications d'entreprises pour
continuer à exister.
</p>

<p>
Même s'il a une expérience de DPL, je pense qu'un temps assez long s'est
écoulé depuis et que beaucoup de choses se sont passées, aussi Debian a
changé de façon significative. Je crois aussi que commencer de façon
négative n'est pas la bonne approche.
</p>
</div>
</div>

<div id="outline-container-org120099b" class="outline-3">
<h3 id="org120099b"><span class="section-number-3">8.4</span> Simon Richter</h3>
<div class="outline-text-3" id="text-8-4">
<p>
Comme nous ne disposons pas de programme, ni même un seul courriel envoyé à
un des fils de discussion depuis le dépôt des candidatures, il n'y a pas de
réfutation possible. Je lui souhaite le meilleur, quoiqu'il en soit
jusqu'à présent.
</p>
</div>
</div>

<div id="outline-container-orgcb30e0c" class="outline-3">
<h3 id="orgcb30e0c"><span class="section-number-3">8.5</span> Commentaire général</h3>
<div class="outline-text-3" id="text-8-5">
<p>
En général tous les programmes disponibles comportent des points positifs 
et tous les candidats qui en ont fourni un, ne sont pas mauvais pour le 
poste, mes préférences personnelles pour le vote, à part ma victoire, 
classent Sam devant Jonathan suivi par Martin.
</p>

<p>
Quel que soit le résultat, je souhaite bonne chance à tous et suis sûr que
le nouveau DPL, quel qu'il soit fera du bon travail pour Debian.
</p>
</div>
</div>
</div>
</div>
<div id="postamble" class="status">
<p class="author">Auteur : Joerg Jaspert</p>
<p class="date">Créé : dimanche 24 mars, 10h14</p>
