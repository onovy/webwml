#use wml::debian::translation-check translation="ab3be4ee01879fd4484c795bbaa824377c218575" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs problèmes ont été découverts dans TIFF, la bibliothèque Tag Image
File Format, qui permettaient à des attaquants distants de
provoquer un déni de service ou un autre impact non précisé à l'aide d'un
fichier image contrefait.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-11613">CVE-2017-11613</a>

<p>Vulnérabilité de déni de service :
une entrée contrefaite peut conduire à une attaque par déni de service. Lors du
processus TIFFOpen, td_imagelength n’est pas vérifié. La valeur de
td_imagelength peut être directement contrôlée par un fichier d’entrée. Dans la
fonction ChopUpSingleUncompressedStrip, la fonction _TIFFCheckMalloc est appelée
en se basant sur td_imagelength. Si la valeur de td_imagelength est réglée
proche du montant de la mémoire système, cela plantera le système ou déclenchera
la fonction OOM-killer.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-10963">CVE-2018-10963</a>

<p>Vulnérabilité de déni de service :
la fonction TIFFWriteDirectorySec() dans tif_dirwrite.c dans LibTIFF permet à
des attaquants distants de provoquer un déni de service (défaut d’assertion et
plantage d'application) à l'aide d'un fichier contrefait. C’est une
vulnérabilité différente de
<a href="https://security-tracker.debian.org/tracker/CVE-2017-13726">CVE-2017-13726</a>.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-5784">CVE-2018-5784</a>

<p>Vulnérabilité de déni de service :
dans LibTIFF, il existe une consommation de ressources incontrôlée dans la
fonction TIFFSetDirectory de tif_dir.c. Des attaquants distants pourraient
exploiter cette vulnérabilité pour provoquer un déni de service à l'aide d'un
fichier tif contrefait. Cela arrive parce que le nombre déclaré d’entrées de
répertoire n’est pas vérifié par rapport au nombre réel d’entrées de répertoire.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-7456">CVE-2018-7456</a>

<p>Déréférencement de pointeur NULL :
un déréférencement de pointeur NULL se produit dans la fonction TIFFPrintDirectory
dans tif_print.c dans LibTIFF lors de l’utilisation de l’outil tiffinfo pour
afficher des informations TIFF contrefaites. C’est une vulnérabilité différente de
<a href="https://security-tracker.debian.org/tracker/CVE-2017-18013">CVE-2017-18013</a>
(cela affecte une partie précédente de la fonction TIFFPrintDirectory qui n’était
pas corrigée par le correctif
<a href="https://security-tracker.debian.org/tracker/CVE-2017-18013">CVE-2017-18013</a>).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-8905">CVE-2018-8905</a>

<p>Dépassement de tampon basé sur le tas :
dans LibTIFF, un dépassement de tampon basé sur le tas se produit dans la
fonction LZWDecodeCompat dans tif_lzw.c à l'aide d'un fichier TIFF contrefait,
comme démontré par tiff2ps.</p></li>

</ul>

<p>Pour Debian 8 <q>Jessie</q>, ces problèmes ont été corrigés dans
la version 4.0.3-12.3+deb8u6.</p>
<p>Nous vous recommandons de mettre à jour vos paquets tiff.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify le following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1411.data"
# $Id: $
