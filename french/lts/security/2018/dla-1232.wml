#use wml::debian::translation-check translation="ce41d997301872adfc27a79ea546429856226b67" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Plusieurs vulnérabilités ont été découvertes dans le noyau Linux qui
pourraient conduire à une augmentation de droits, un déni de service ou à
des fuites d'informations.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-5754">CVE-2017-5754</a>

<p>Plusieurs chercheurs ont découvert une vulnérabilité dans les
processeurs Intel, permettant à un attaquant contrôlant un processus non
privilégié de lire la mémoire à partir d'adresses arbitraires, y compris à
partir du noyau et de tous les autres processus en cours d’exécution sur le
système.</p>

<p>Cette attaque particulière a été appelée Meltdown et est traitée dans le
noyau Linux pour l'architecture Intel x86-64 par un ensemble de correctifs
nommés « Kernel Page Table Isolation », imposant une séparation presque
complète entre les mappages d'adresses du noyau et de l'espace utilisateur
et empêchant l'attaque. Cette solution peut avoir un impact en termes de
performance, et elle peut être désactivée lors de l'amorçage en passant le
code <code>pti=off</code> sur la ligne de commande du noyau.</p>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-17558">CVE-2017-17558</a>

<p>Andrey Konovalov a signalé que le noyau USB ne gérait pas correctement
certaines conditions d'erreur durant l'initialisation. Un utilisateur
physiquement présent, avec un périphérique USB spécialement conçu, peut
utiliser cela pour provoquer un déni de service (plantage ou corruption de
mémoire), ou éventuellement une augmentation de droits.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-17741">CVE-2017-17741</a>

<p>Dmitry Vyukov a signalé que l'implémentation de KVM pour x86 pourrait
lire des données de la mémoire au-delà des limites lors de l'émulation
d'une écriture MMIO si le point de trace de kvm_mmio était activé. Une
machine virtuelle cliente pourrait être capable d'utiliser cela pour
provoquer un déni de service (plantage).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-17805">CVE-2017-17805</a>

<p>Certaines implémentations du chiffrement par bloc Salsa20 ne géraient
pas correctement les entrées de longueur nulle. Un utilisateur local
pourrait utiliser cela pour provoquer un déni de service (plantage) ou
éventuellement avoir un autre impact de sécurité.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-17806">CVE-2017-17806</a>

<p>L'implémentation de HMAC pourrait être utilisée avec un algorithme de
hachage sous-jacent qui requiert une clé, ce qui n'était pas voulu. Un
utilisateur local pourrait utiliser cela pour provoquer un déni de service
(plantage ou corruption de mémoire), ou éventuellement pour une
augmentation de droits.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-17807">CVE-2017-17807</a>

<p>Eric Biggers a découvert que le sous-système KEYS ne vérifiait pas les
droits d'écriture lors de l'addition de clés à un trousseau par défaut d'un
processus. Un utilisateur local pourrait utiliser cela pour provoquer un
déni de service ou pour obtenir des informations sensibles.</p></li>

</ul>

<p>Pour Debian 7 <q>Wheezy</q>, ces problèmes ont été corrigés dans la
version 3.2.96-3.</p>

<p>Nous vous recommandons de mettre à jour vos paquets linux.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1232.data"
# $Id: $
