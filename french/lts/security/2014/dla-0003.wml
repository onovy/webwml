#use wml::debian::translation-check translation="0489e1b952f47155cfd52286f4b52319011dbc06" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2014-0195">CVE-2014-0195</a>

<p>Jueri Aedla a découvert qu'un dépassement de tampon dans le traitement
des fragments DTLS pourrait conduire à l'exécution de code arbitraire ou à
un déni de service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2014-0221">CVE-2014-0221</a>

<p>Imre Rad a découvert que le traitement de paquets hello DTLS était
vulnérable à un déni de service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2014-0224">CVE-2014-0224</a>

<p>KIKUCHI Masashi a découvert que des négociations de connexion
soigneusement contrefaites pouvaient obliger à l'utilisation de clés
faibles, résultant dans de potentielles attaques du type « homme du
milieu ».</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2014-3470">CVE-2014-3470</a>

<p>Felix Groebert et Ivan Fratric ont découvert que l'implémentation des
suites de chiffrement ECDH anonymes est vulnérable à un déni de service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2014-0076">CVE-2014-0076</a>

<p>Correctif pour l'attaque décrite dans l'article « Recovering
 OpenSSL ECDSA Nonces Using the FLUSH+RELOAD Cache Side-channel Attack »
rapportée par Yuval Yarom et Naomi Benger.</p></li>

</ul>

<p>Des informations supplémentaires peuvent être trouvées à l'adresse
<a href="http://www.openssl.org/news/secadv_20140605.txt">\
http://www.openssl.org/news/secadv_20140605.txt</a>.</p>

<p>Toutes les applications liées à openssl doivent être redémarrées. Vous
pouvez utiliser l'outil checkrestart du paquet debian-goodies pour détecter
les programmes affectés ou redémarrer votre système.</p>

<p>Il est important de mettre à niveau le paquet libssl0.9.8 et pas
uniquement le paquet openssl.</p>

<p>Pour Debian 6 <q>Squeeze</q>, ces problèmes ont été corrigés dans la
version 0.9.8o-4squeeze15 d'openssl.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2014/dla-0003.data"
# $Id: $
