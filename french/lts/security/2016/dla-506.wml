#use wml::debian::translation-check translation="adc5cbd36ecf754028e80bbdee567a58bca03b81" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Deux vulnérabilités ont été découvertes dans dhcpcd5, un paquet de
client DHCP. Un attaquant distant (sur une réseau local) peut
éventuellement exécuter du code arbitraire ou provoquer une attaque par
déni de service à l'aide de messages contrefaits.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2014-7912">CVE-2014-7912</a>

<p>La fonction get_option ne valide pas la relation entre champs de
longueur et quantité de données. Cela permet à des serveurs DHCP distants
d'exécuter du code arbitraire ou de provoquer un déni de service
(corruption de mémoire) à l'aide d'une valeur de longueur importante d'une
option dans un message DHCPACK.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2014-7913">CVE-2014-7913</a>

<p>La fonction print_option interprète mal la valeur de retour de la
fonction snprintf. Cela permet à des serveurs DHCP distants d'exécuter du
code arbitraire ou de provoquer un déni de service (corruption de mémoire)
à l'aide d'un message contrefait.</p></li>

</ul>

<p>Pour Debian 7 <q>Wheezy</q>, ces problèmes ont été corrigés dans la
version 5.5.6-1+deb7u2.</p>

<p>Nous vous recommandons de mettre à jour vos paquets dhcpcd5.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-506.data"
# $Id: $
