#use wml::debian::translation-check translation="b061a3eb5a284dce5a149a8505b3121bd7e4c6b6" maintainer="Sebul"
<define-tag pagetitle>DebConf19 Curitiba에서 끝나고 DebConf20 날짜 알림</define-tag>

<define-tag release_date>2019-07-27</define-tag>
#use wml::debian::news


<p>오늘, 2019년 7월 27일 토요일, 연례 데비안 개발자 및 기여자 컨퍼런스를 마쳤습니다.
총 145개의 이벤트 회의, 토론 세션, BoF(Bird of Feather) 모임, 워크샵 및 활동을 통해 50개국에서 380명 이상의 참석자를 유치합니다.
<a href="https://debconf19.debconf.org">DebConf19</a>는 대성공입니다.
</p>

<p>
The conference was preceded by the annual DebCamp held 14 July to 19 July
which focused on individual work and team sprints for in-person collaboration
toward developing Debian and host to a 3-day packaging workshop
where new contributors were able to start on Debian packaging.
</p>

<p>
The <a href="https://debconf19.debconf.org/news/2019-07-20-open-day/">Open Day</a>
held on July 20, with over 250 attendees, enjoyed presentations
and workshops of interest to the wider audience, a Job Fair with booths from
several of the DebConf19 sponsors and a Debian install fest.
</p>

<p>
The actual Debian Developers Conference started on Sunday 21 July 2019.
Together with plenaries such as the the traditional 'Bits from the DPL',
lightning talks, live demos
and the announcement of next year's DebConf
(<a href="https://wiki.debian.org/DebConf/20">DebConf20</a> in Haifa, Israel), 
there were several sessions related to the recent release of Debian 10 buster
and some of its new features, as well as news updates on several projects and 
internal Debian teams, discussion sessions (BoFs) from the language, ports,
infrastructure, and  community teams, 
along with many other events of interest regarding Debian and free software.
</p>

<p>
The <a href="https://debconf19.debconf.org/schedule/">schedule</a>
was updated each day with planned and ad-hoc activities introduced by attendees
over the course of the entire conference.
</p>

<p>
For those who were not able to attend, most of the talks and sessions were 
recorded for live streams with videos made,
available through the
<a href="https://meetings-archive.debian.net/pub/debian-meetings/2019/DebConf19/">Debian meetings archive website</a>.
Almost all of the sessions facilitated remote participation via IRC messaging 
apps or online collaborative text documents.
</p>

<p>
The <a href="https://debconf19.debconf.org/">DebConf19 website</a>
will remain active for archival purposes and will continue to offer
links to the presentations and videos of talks and events.
</p>

<p>
Next year, <a href="https://wiki.debian.org/DebConf/20">DebConf20</a> will be held in Haifa, Israel,
from 23 August to 29 August 2020. 
As tradition follows before the next DebConf the local organizers in Israel 
will start the conference activites with DebCamp (16 August to 22 August),
with particular focus on individual and team work toward improving the 
distribution.
</p>

<p>
DebConf is committed to a safe and welcome environment for all participants.
During the conference, several teams (Front Desk, Welcome team and Anti-Harassment team)
are available to help so both on-site and remote participants get their best experience
in the conference, and find solutions to any issue that may arise.
See the <a href="https://debconf19.debconf.org/about/coc/">web page about the Code of Conduct in DebConf19 website</a>
for more details on this.
</p>

<p>
Debian thanks the commitment of numerous <a href="https://debconf19.debconf.org/sponsors/">sponsors</a>
to support DebConf19, particularly our Platinum Sponsors:
<a href="https://www.infomaniak.com">Infomaniak</a>,
<a href="https://google.com/">Google</a>
and <a href="https://www.lenovo.com">Lenovo</a>.
</p>

<h2>About Debian</h2>

<p>
The Debian Project was founded in 1993 by Ian Murdock to be a truly
free community project. Since then the project has grown to be one of
the largest and most influential open source projects.  Thousands of
volunteers from all over the world work together to create and
maintain Debian software. Available in 70 languages, and
supporting a huge range of computer types, Debian calls itself the
<q>universal operating system</q>.
</p>

<h2>DebConf는</h2>

<p>
DebConf is the Debian Project's developer conference. In addition to a
full schedule of technical, social and policy talks, DebConf provides an
opportunity for developers, contributors and other interested people to
meet in person and work together more closely. It has taken place
annually since 2000 in locations as varied as Scotland, Argentina, and
Bosnia and Herzegovina. More information about DebConf is available from
<a href="https://debconf.org/">https://debconf.org</a>.
</p>

<h2>Infomaniak은</h2>

<p>
<a href="https://www.infomaniak.com">Infomaniak</a> is Switzerland's largest web-hosting company,
also offering backup and storage services, solutions for event organizers,
live-streaming and video on demand services.
It wholly owns its datacenters and all elements critical 
to the functioning of the services and products provided by the company 
(both software and hardware). 
</p>

<h2>Google은</h2>

<p>
<a href="https://google.com/">Google</a> is one of the largest technology companies in the
world, providing a wide range of Internet-related services and products such
as online advertising technologies, search, cloud computing, software, and hardware.
</p>

<p>
Google has been supporting Debian by sponsoring DebConf for more than
ten years, and is also a Debian partner sponsoring parts 
of <a href="https://salsa.debian.org">Salsa</a>'s continuous integration infrastructure
within Google Cloud Platform.
</p>

<h2>Lenovo는</h2>

<p>
As a global technology leader manufacturing a wide portfolio of connected products,
including smartphones, tablets, PCs and workstations as well as AR/VR devices,
smart home/office and data center solutions, <a href="https://www.lenovo.com">Lenovo</a>
understands how critical open systems and platforms are to a connected world.
</p>

<h2>연락 정보</h2>

<p>For further information, please visit the DebConf19 web page at
<a href="https://debconf19.debconf.org/">https://debconf19.debconf.org/</a>
or send mail to &lt;press@debian.org&gt;.</p>
