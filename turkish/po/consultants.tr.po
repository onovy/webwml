# Turkish messages for Debian Web pages (debian-webwml).
# Copyright (C) 2003, 2004 Software in the Public Interest, Inc.
# This file is distributed under the same license as Debian Web pages.
#
# Murat Demirten <murat@debian.org>, 2002, 2003, 2004.
# Recai Oktaş <roktas@omu.edu.tr>, 2005.
#
msgid ""
msgstr ""
"Project-Id-Version: debian-webwml\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2003-11-10 15:06+0100\n"
"PO-Revision-Date: 2019-01-17 23:52+0100\n"
"Last-Translator: Laura Arjona Reina <larjona@debian.org>\n"
"Language-Team: Debian L10n Turkish <debian-l10n-turkish@lists.debian.org>\n"
"Language: tr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: Poedit 1.8.11\n"

#: ../../english/template/debian/consultant.wml:6
msgid "List of Consultants"
msgstr "Danışman Listesi"

#: ../../english/template/debian/consultant.wml:9
msgid "Back to the <a href=\"./\">Debian consultants page</a>."
msgstr "<a href=\"./\">Debian danışmanlar sayfası</a>'na dönüş."

#: ../../english/consultants/consultant.defs:6
msgid "Name:"
msgstr "İsim:"

#: ../../english/consultants/consultant.defs:9
msgid "Company:"
msgstr "Şirket:"

#: ../../english/consultants/consultant.defs:12
msgid "Address:"
msgstr "Adres:"

#: ../../english/consultants/consultant.defs:15
msgid "Contact:"
msgstr "İletişim:"

#: ../../english/consultants/consultant.defs:19
msgid "Phone:"
msgstr "Telefon:"

#: ../../english/consultants/consultant.defs:22
msgid "Fax:"
msgstr "Faks:"

#: ../../english/consultants/consultant.defs:25
msgid "URL:"
msgstr "URL:"

#: ../../english/consultants/consultant.defs:29
msgid "or"
msgstr "veya"

#: ../../english/consultants/consultant.defs:34
msgid "Email:"
msgstr "Eposta:"

#: ../../english/consultants/consultant.defs:52
msgid "Rates:"
msgstr "Tarifeler:"

#: ../../english/consultants/consultant.defs:55
msgid "Additional Information"
msgstr "Ek Bilgiler"

#: ../../english/consultants/consultant.defs:58
msgid "Willing to Relocate"
msgstr "Herhangi bir yerde hizmet verebilecekler"

#: ../../english/consultants/consultant.defs:61
msgid ""
"<total_consultant> Debian consultants listed in <total_country> countries "
"worldwide."
msgstr ""
"<total_country> ülkede toplam <total_consultant> Debian danışmanı "
"bulunmaktadır."
