msgid ""
msgstr ""
"Project-Id-Version: Debian webwml organization\n"
"PO-Revision-Date: 2006-08-12 16:57+0200\n"
"Last-Translator: Josip Rodin\n"
"Language-Team: Croatian\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../../english/template/debian/votebar.wml:13
msgid "Date"
msgstr "Datum"

#: ../../english/template/debian/votebar.wml:16
msgid "Time Line"
msgstr "Vremenski tok"

#: ../../english/template/debian/votebar.wml:19
msgid "Summary"
msgstr ""

#: ../../english/template/debian/votebar.wml:22
msgid "Nominations"
msgstr "Nominacije"

#: ../../english/template/debian/votebar.wml:25
#, fuzzy
msgid "Withdrawals"
msgstr "Povučeno"

#: ../../english/template/debian/votebar.wml:28
msgid "Debate"
msgstr "Rasprava"

#: ../../english/template/debian/votebar.wml:31
msgid "Platforms"
msgstr "Platforme"

#: ../../english/template/debian/votebar.wml:34
msgid "Proposer"
msgstr "Predlagač"

#: ../../english/template/debian/votebar.wml:37
msgid "Proposal A Proposer"
msgstr "Predlagač prijedloga A"

#: ../../english/template/debian/votebar.wml:40
msgid "Proposal B Proposer"
msgstr "Predlagač prijedloga B"

#: ../../english/template/debian/votebar.wml:43
msgid "Proposal C Proposer"
msgstr "Predlagač prijedloga C"

#: ../../english/template/debian/votebar.wml:46
msgid "Proposal D Proposer"
msgstr "Predlagač prijedloga D"

#: ../../english/template/debian/votebar.wml:49
msgid "Proposal E Proposer"
msgstr "Predlagač prijedloga E"

#: ../../english/template/debian/votebar.wml:52
msgid "Proposal F Proposer"
msgstr "Predlagač prijedloga F"

#: ../../english/template/debian/votebar.wml:55
#, fuzzy
msgid "Proposal G Proposer"
msgstr "Predlagač prijedloga A"

#: ../../english/template/debian/votebar.wml:58
#, fuzzy
msgid "Proposal H Proposer"
msgstr "Predlagač prijedloga A"

#: ../../english/template/debian/votebar.wml:61
msgid "Seconds"
msgstr "Podržavaju"

#: ../../english/template/debian/votebar.wml:64
msgid "Proposal A Seconds"
msgstr "Prijedlog A podržavaju"

#: ../../english/template/debian/votebar.wml:67
msgid "Proposal B Seconds"
msgstr "Prijedlog B podržavaju"

#: ../../english/template/debian/votebar.wml:70
msgid "Proposal C Seconds"
msgstr "Prijedlog C podržavaju"

#: ../../english/template/debian/votebar.wml:73
msgid "Proposal D Seconds"
msgstr "Prijedlog D podržavaju"

#: ../../english/template/debian/votebar.wml:76
msgid "Proposal E Seconds"
msgstr "Prijedlog E podržavaju"

#: ../../english/template/debian/votebar.wml:79
msgid "Proposal F Seconds"
msgstr "Prijedlog F podržavaju"

#: ../../english/template/debian/votebar.wml:82
#, fuzzy
msgid "Proposal G Seconds"
msgstr "Prijedlog A podržavaju"

#: ../../english/template/debian/votebar.wml:85
#, fuzzy
msgid "Proposal H Seconds"
msgstr "Prijedlog A podržavaju"

#: ../../english/template/debian/votebar.wml:88
msgid "Opposition"
msgstr "Protivljenje"

#: ../../english/template/debian/votebar.wml:91
msgid "Text"
msgstr "Tekst"

#: ../../english/template/debian/votebar.wml:94
msgid "Proposal A"
msgstr "Prijedlog A"

#: ../../english/template/debian/votebar.wml:97
msgid "Proposal B"
msgstr "Prijedlog B"

#: ../../english/template/debian/votebar.wml:100
msgid "Proposal C"
msgstr "Prijedlog C"

#: ../../english/template/debian/votebar.wml:103
msgid "Proposal D"
msgstr "Prijedlog D"

#: ../../english/template/debian/votebar.wml:106
msgid "Proposal E"
msgstr "Prijedlog E"

#: ../../english/template/debian/votebar.wml:109
msgid "Proposal F"
msgstr "Prijedlog F"

#: ../../english/template/debian/votebar.wml:112
#, fuzzy
msgid "Proposal G"
msgstr "Prijedlog A"

#: ../../english/template/debian/votebar.wml:115
#, fuzzy
msgid "Proposal H"
msgstr "Prijedlog A"

#: ../../english/template/debian/votebar.wml:118
msgid "Choices"
msgstr "Izbor"

#: ../../english/template/debian/votebar.wml:121
msgid "Amendment Proposer"
msgstr "Predlagač amandmana"

#: ../../english/template/debian/votebar.wml:124
msgid "Amendment Seconds"
msgstr "Amandman podržavaju"

#: ../../english/template/debian/votebar.wml:127
msgid "Amendment Text"
msgstr "Tekst amandmana"

#: ../../english/template/debian/votebar.wml:130
msgid "Amendment Proposer A"
msgstr "Amandman A predlaže"

#: ../../english/template/debian/votebar.wml:133
msgid "Amendment Seconds A"
msgstr "Amandman A podržavaju"

#: ../../english/template/debian/votebar.wml:136
msgid "Amendment Text A"
msgstr "Tekst amandmana A"

#: ../../english/template/debian/votebar.wml:139
msgid "Amendment Proposer B"
msgstr "Amandman B predlaže"

#: ../../english/template/debian/votebar.wml:142
msgid "Amendment Seconds B"
msgstr "Amandman B podržavaju"

#: ../../english/template/debian/votebar.wml:145
msgid "Amendment Text B"
msgstr "Tekst amandmana B"

#: ../../english/template/debian/votebar.wml:148
#, fuzzy
msgid "Amendment Proposer C"
msgstr "Amandman A predlaže"

#: ../../english/template/debian/votebar.wml:151
#, fuzzy
msgid "Amendment Seconds C"
msgstr "Amandman A podržavaju"

#: ../../english/template/debian/votebar.wml:154
#, fuzzy
msgid "Amendment Text C"
msgstr "Tekst amandmana A"

#: ../../english/template/debian/votebar.wml:157
msgid "Amendments"
msgstr "Amandmani"

#: ../../english/template/debian/votebar.wml:160
msgid "Proceedings"
msgstr "Postupak"

#: ../../english/template/debian/votebar.wml:163
msgid "Majority Requirement"
msgstr "Zahtjevi za većinu"

#: ../../english/template/debian/votebar.wml:166
msgid "Data and Statistics"
msgstr "Podaci i statistike"

#: ../../english/template/debian/votebar.wml:169
msgid "Quorum"
msgstr "Kvorum"

#: ../../english/template/debian/votebar.wml:172
msgid "Minimum Discussion"
msgstr "Minimalna rasprava"

#: ../../english/template/debian/votebar.wml:175
msgid "Ballot"
msgstr "Glasački list"

#: ../../english/template/debian/votebar.wml:178
msgid "Forum"
msgstr "Forum"

#: ../../english/template/debian/votebar.wml:181
msgid "Outcome"
msgstr "Rezultat"

#: ../../english/template/debian/votebar.wml:185
msgid "Waiting&nbsp;for&nbsp;Sponsors"
msgstr "Čeka&nbsp;na&nbsp;pokrovitelje"

#: ../../english/template/debian/votebar.wml:188
msgid "In&nbsp;Discussion"
msgstr "U&nbsp;raspravi"

#: ../../english/template/debian/votebar.wml:191
msgid "Voting&nbsp;Open"
msgstr "Glasovanje&nbsp;otvoreno"

#: ../../english/template/debian/votebar.wml:194
msgid "Decided"
msgstr "Odlučeno"

#: ../../english/template/debian/votebar.wml:197
msgid "Withdrawn"
msgstr "Povučeno"

#: ../../english/template/debian/votebar.wml:200
msgid "Other"
msgstr "Ostalo"

#: ../../english/template/debian/votebar.wml:204
msgid "Home&nbsp;Vote&nbsp;Page"
msgstr "Početna&nbsp;stranica&nbsp;glasovanja"

#: ../../english/template/debian/votebar.wml:207
msgid "How&nbsp;To"
msgstr "Kako"

#: ../../english/template/debian/votebar.wml:210
msgid "Submit&nbsp;a&nbsp;Proposal"
msgstr "Predati&nbsp;prijedlog"

#: ../../english/template/debian/votebar.wml:213
msgid "Amend&nbsp;a&nbsp;Proposal"
msgstr "Ispraviti&nbsp;prijedlog"

#: ../../english/template/debian/votebar.wml:216
msgid "Follow&nbsp;a&nbsp;Proposal"
msgstr "Slijediti&nbsp;prijedlog"

#: ../../english/template/debian/votebar.wml:219
msgid "Read&nbsp;a&nbsp;Result"
msgstr "Pročitati&nbsp;rezultat"

#: ../../english/template/debian/votebar.wml:222
msgid "Vote"
msgstr "Glasovati"
