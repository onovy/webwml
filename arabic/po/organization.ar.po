# translation of organization.po to Arabic
# Isam Bayazidi <isam@bayazidi.net>, 2004, 2005.
# Ossama M. Khayat <okhayat@yahoo.com>, 2005.
# Mohamed Amine <med@mailoo.org>, 2013.
msgid ""
msgstr ""
"Project-Id-Version: organization.ar\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2013-05-08 17:17+0000\n"
"PO-Revision-Date: 2013-05-08 17:23+0000\n"
"Last-Translator: Mohamed Amine <med@mailoo.org>\n"
"Language-Team: Arabic <doc@arabeyes.org>\n"
"Language: ar\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=6; plural=n==0 ? 0 : n==1 ? 1 : n==2 ? 2 : n%100>=3 "
"&& n%100<=10 ? 3 : n%100>=11 ? 4 : 5;\n"
"X-Generator: Virtaal 0.7.1\n"

#: ../../english/intro/organization.data:15
msgid "delegation mail"
msgstr ""

#: ../../english/intro/organization.data:16
msgid "appointment mail"
msgstr ""

#. One male delegate
#: ../../english/intro/organization.data:18
msgid "<void id=\"male\"/>delegate"
msgstr ""

#. One female delegate
#: ../../english/intro/organization.data:20
msgid "<void id=\"female\"/>delegate"
msgstr ""

#: ../../english/intro/organization.data:23
#: ../../english/intro/organization.data:25
msgid "current"
msgstr "الحالي"

#: ../../english/intro/organization.data:27
#: ../../english/intro/organization.data:29
msgid "member"
msgstr "عضو"

#: ../../english/intro/organization.data:32
msgid "manager"
msgstr "مدير"

#: ../../english/intro/organization.data:34
msgid "Stable Release Manager"
msgstr ""

#: ../../english/intro/organization.data:34
msgid "SRM"
msgstr ""

#: ../../english/intro/organization.data:36
msgid "wizard"
msgstr ""

#. we only use the chair tag once, for techctte, I wonder why it's here.
#: ../../english/intro/organization.data:38
#, fuzzy
msgid "chair"
msgstr "رئيس المجلس"

#: ../../english/intro/organization.data:41
msgid "assistant"
msgstr ""

#: ../../english/intro/organization.data:43
msgid "secretary"
msgstr "أمين عام"

#: ../../english/intro/organization.data:45
msgid "representative"
msgstr ""

#: ../../english/intro/organization.data:47
msgid "role"
msgstr ""

#: ../../english/intro/organization.data:63
#: ../../english/intro/organization.data:75
msgid "Officers"
msgstr "أعضاء"

#: ../../english/intro/organization.data:64
#: ../../english/intro/organization.data:99
msgid "Distribution"
msgstr "توزيعة"

#: ../../english/intro/organization.data:65
#: ../../english/intro/organization.data:235
msgid "Communication and Outreach"
msgstr ""

#: ../../english/intro/organization.data:67
#: ../../english/intro/organization.data:238
msgid "Data Protection team"
msgstr ""

#: ../../english/intro/organization.data:68
#: ../../english/intro/organization.data:242
#, fuzzy
msgid "Publicity team"
msgstr "الدعاية والإعلان"

#: ../../english/intro/organization.data:70
#: ../../english/intro/organization.data:311
msgid "Membership in other organizations"
msgstr ""

#: ../../english/intro/organization.data:71
#: ../../english/intro/organization.data:339
msgid "Support and Infrastructure"
msgstr "دعم فني وبنية تحتية"

#: ../../english/intro/organization.data:78
msgid "Leader"
msgstr "قائد"

#: ../../english/intro/organization.data:80
msgid "Technical Committee"
msgstr "اللجنة التقنية"

#: ../../english/intro/organization.data:94
msgid "Secretary"
msgstr "أمين عام"

#: ../../english/intro/organization.data:102
msgid "Development Projects"
msgstr "مشاريع التطوير"

#: ../../english/intro/organization.data:103
msgid "FTP Archives"
msgstr "أرشيف FTP "

#: ../../english/intro/organization.data:105
msgid "FTP Masters"
msgstr ""

#: ../../english/intro/organization.data:111
msgid "FTP Assistants"
msgstr "مساعد FTP"

#: ../../english/intro/organization.data:116
msgid "FTP Wizards"
msgstr ""

#: ../../english/intro/organization.data:120
msgid "Backports"
msgstr ""

#: ../../english/intro/organization.data:122
msgid "Backports Team"
msgstr ""

#: ../../english/intro/organization.data:126
msgid "Release Management"
msgstr "إدارة الإصدارات"

#: ../../english/intro/organization.data:128
msgid "Release Team"
msgstr ""

#: ../../english/intro/organization.data:141
msgid "Quality Assurance"
msgstr "ضمان الجودة"

#: ../../english/intro/organization.data:142
msgid "Installation System Team"
msgstr "فريق نظام التثبيت"

#: ../../english/intro/organization.data:143
msgid "Debian Live Team"
msgstr ""

#: ../../english/intro/organization.data:144
msgid "Release Notes"
msgstr "ملاحظات الإصدارة"

#: ../../english/intro/organization.data:146
msgid "CD Images"
msgstr "ملفات قرص مدمج"

#: ../../english/intro/organization.data:148
msgid "Production"
msgstr "الإنتاج"

#: ../../english/intro/organization.data:156
msgid "Testing"
msgstr "اختبار"

#: ../../english/intro/organization.data:158
msgid "Cloud Team"
msgstr ""

#: ../../english/intro/organization.data:162
msgid "Autobuilding infrastructure"
msgstr ""

#: ../../english/intro/organization.data:164
msgid "Wanna-build team"
msgstr ""

#: ../../english/intro/organization.data:171
msgid "Buildd administration"
msgstr ""

#: ../../english/intro/organization.data:189
msgid "Documentation"
msgstr "التوثيق"

#: ../../english/intro/organization.data:194
msgid "Work-Needing and Prospective Packages list"
msgstr "قائمة الحزم التي هي بحاجة إلى مزيد من العمل والمتوقعة"

#: ../../english/intro/organization.data:196
msgid "Ports"
msgstr "تحويلات"

#: ../../english/intro/organization.data:226
msgid "Special Configurations"
msgstr "إعدادات خاصة"

#: ../../english/intro/organization.data:228
msgid "Laptops"
msgstr "الحواسب المحمولة"

#: ../../english/intro/organization.data:229
msgid "Firewalls"
msgstr "الجدران النارية"

#: ../../english/intro/organization.data:230
msgid "Embedded systems"
msgstr "الأجهزة المدمجة"

#: ../../english/intro/organization.data:245
msgid "Press Contact"
msgstr "جهة الإتصال الصحفي"

#: ../../english/intro/organization.data:247
msgid "Web Pages"
msgstr "صفحات الإنترنت"

#: ../../english/intro/organization.data:259
msgid "Planet Debian"
msgstr ""

#: ../../english/intro/organization.data:264
msgid "Outreach"
msgstr ""

#: ../../english/intro/organization.data:269
msgid "Debian Women Project"
msgstr ""

#: ../../english/intro/organization.data:277
msgid "Anti-harassment"
msgstr ""

#: ../../english/intro/organization.data:282
msgid "Events"
msgstr "أحداث"

#: ../../english/intro/organization.data:289
#, fuzzy
msgid "DebConf Committee"
msgstr "اللجنة التقنية"

#: ../../english/intro/organization.data:296
msgid "Partner Program"
msgstr "برامج الشراكة"

#: ../../english/intro/organization.data:301
msgid "Hardware Donations Coordination"
msgstr "تنسيق التبرعات بالعتاد والأجهزة"

#: ../../english/intro/organization.data:317
msgid "GNOME Foundation"
msgstr ""

#: ../../english/intro/organization.data:319
msgid "Linux Professional Institute"
msgstr ""

#: ../../english/intro/organization.data:321
msgid "Linux Magazine"
msgstr ""

#: ../../english/intro/organization.data:323
msgid "Linux Standards Base"
msgstr ""

#: ../../english/intro/organization.data:325
msgid "Free Standards Group"
msgstr ""

#: ../../english/intro/organization.data:326
msgid "SchoolForge"
msgstr ""

#: ../../english/intro/organization.data:329
msgid ""
"OASIS: Organization\n"
"      for the Advancement of Structured Information Standards"
msgstr ""

#: ../../english/intro/organization.data:332
msgid ""
"OVAL: Open Vulnerability\n"
"      Assessment Language"
msgstr ""

#: ../../english/intro/organization.data:335
msgid "Open Source Initiative"
msgstr ""

#: ../../english/intro/organization.data:342
msgid "User support"
msgstr "دعم المستخدم"

#: ../../english/intro/organization.data:409
msgid "Bug Tracking System"
msgstr "نظام تتبع العلاّت"

#: ../../english/intro/organization.data:414
msgid "Mailing Lists Administration and Mailing List Archives"
msgstr ""

#: ../../english/intro/organization.data:422
msgid "New Members Front Desk"
msgstr ""

#: ../../english/intro/organization.data:428
msgid "Debian Account Managers"
msgstr "مدراء حسابات دبيان"

#: ../../english/intro/organization.data:432
msgid ""
"To send a private message to all DAMs, use the GPG key "
"57731224A9762EA155AB2A530CA8D15BB24D96F2."
msgstr ""

#: ../../english/intro/organization.data:433
msgid "Keyring Maintainers (PGP and GPG)"
msgstr "مشرفو مفاتيح التشفير PGP و GPG"

#: ../../english/intro/organization.data:437
msgid "Security Team"
msgstr "الفريق الأمني"

#: ../../english/intro/organization.data:448
msgid "Consultants Page"
msgstr "صفحة المستشارين"

#: ../../english/intro/organization.data:453
msgid "CD Vendors Page"
msgstr "صفحة موزعي الأقراص المدمجة"

#: ../../english/intro/organization.data:456
msgid "Policy"
msgstr "سياسة"

#: ../../english/intro/organization.data:459
msgid "System Administration"
msgstr "إدارة النظام"

#: ../../english/intro/organization.data:460
msgid ""
"This is the address to use when encountering problems on one of Debian's "
"machines, including password problems or you need a package installed."
msgstr ""
"هذا هو العنوان الذي يجب استخدامه عند مواجهة مشاكل اتصال في أحد أجهزة مشروع "
"دبيان، وهذا يشمل المشاكل مع الدخول وكلمة السر، والحصول على حزمة معينة "
"للتثبيت."

#: ../../english/intro/organization.data:469
msgid ""
"If you have hardware problems with Debian machines, please see <a href="
"\"https://db.debian.org/machines.cgi\">Debian Machines</a> page, it should "
"contain per-machine administrator information."
msgstr ""
"إذا واجهت مشاكل مع العتاد في أحد أجهزة دبيان، يرجى مراجعة <a href=\"https://"
"db.debian.org/machines.cgi\">أجهزة دبيان</a> للحصول على معلومات الإتصال "
"الخاصة بالمدير المسؤول عن الجهاز."

#: ../../english/intro/organization.data:470
msgid "LDAP Developer Directory Administrator"
msgstr "مدير دليل LDAP لمعلومات المطورين"

#: ../../english/intro/organization.data:471
msgid "Mirrors"
msgstr "المرايا"

#: ../../english/intro/organization.data:478
msgid "DNS Maintainer"
msgstr "مشرف ال DNS"

#: ../../english/intro/organization.data:479
msgid "Package Tracking System"
msgstr "نظام تتبع الحزم"

#: ../../english/intro/organization.data:481
msgid "Treasurer"
msgstr ""

#: ../../english/intro/organization.data:488
msgid ""
"<a name=\"trademark\" href=\"m4_HOME/trademark\">Trademark</a> use requests"
msgstr ""

#: ../../english/intro/organization.data:491
#, fuzzy
msgid "Salsa administrators"
msgstr "مدراء اولاين"

#~ msgid "Debian for children from 1 to 99"
#~ msgstr "ديبيان للأطفال من سنة الى 99 سنوات"

#~ msgid "Debian for medical practice and research"
#~ msgstr "دبيان للأغراض الطبية والبحث الطبي"

#~ msgid "Debian for education"
#~ msgstr "دبيان للتعليم"

#~ msgid "Debian in legal offices"
#~ msgstr "دبيان في المكاتب القانونية"

#~ msgid "Debian for people with disabilities"
#~ msgstr "دبيان لدوي الاحتياجات الخاصة"

#, fuzzy
#~| msgid "Debian for education"
#~ msgid "Debian for astronomy"
#~ msgstr "دبيان للتعليم"

#~ msgid "Alioth administrators"
#~ msgstr "مدراء اولاين"

#~ msgid "Security Audit Project"
#~ msgstr "مشروع التدقيق الأمني"

#~ msgid "Publicity"
#~ msgstr "الدعاية والإعلان"

#~ msgid "Individual Packages"
#~ msgstr "حزم منفردة"
