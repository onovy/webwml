#use wml::debian::template title="Debian 新成员专区" BARETITLE="true"
#use wml::debian::translation-check translation="cc0db4d4087a4f97b1a3955e3ca0f84b31caf8a9"

<p>Debian 新成员过程是成为正式 Debian 开发人员（DD）的过程。这些网页是为准 Debian \
开发人员提供有关如何申请成为 DD，申请过程的不同步骤，以及如何查看正在进行的申请\
程序的进度这些详细信息的地方。</p>

<p>首先要说明的重点是您<em>不</em>需要成为正式 Debian 开发人员也可帮助改善 Debian。\
实际上，在您申请新成员过程前，您应该已经有更早的对 Debian 贡献的记录。</p>

<p><a name="non-maintainer-contributions"></a>Debian 是一个开放社区，欢迎任何想要使用 Debian \
或帮助改善我们分发的人。作为非开发人员，您可以：</p>

<ul> 
  <li>通过<a href="#Sponsor">赞助者</a>维护软件包</li>
  <li>创建和/或检查翻译</li>
  <li>创建或改善文档</li>
  <li><a href="../website">帮助维护网站</a></li>
  <li>帮助处理错误（通过提交补丁，汇报有价值的 bug，确认 bug 的存在，
    寻找复现问题的方法，......）</li>
  <li>成为软件包团队的活跃成员（例如，debian-qt-kde 或 debian-gnome）</li>
  <li>成为子项目的活跃成员（例如，debian-installer 或 debian-desktop）</li>
  <li>等等</li> 
</ul>

<p><a href="$(DOC)/developers-reference/new-maintainer.html">Debian开发人员\
参考手册</a>包含如何完成一部分这些任务（尤其是如何找到愿意帮您的赞助者）的\
一些具体建议。</p>

<p>Debian 新成员过程是成为正式 Debian 开发人员（DD）的过程。DD 是 Debian 中的传统\
正式成员。DD 能参与 Debian 选举。负责打包的 DD 能将任何软件包上传到软件档案库。\
在申请成为负责打包的 DD 之前，您应有至少六个月的打包维护记录。例如：作为一个\ 
<a href="https://wiki.debian.org/DebianMaintainer">Debian 维护者（DM）</a>上传\
软件包，在团队内部工作或维护由赞助者上传的软件包。非负责打包的 DD 在软件档案库\
中没有权限。在申请成为非负责打包的 DD 之前，您应该在项目中具有可见且重要的工作\
记录。</p>

<p>重要的是要了解到，新成员过程是 Debian 质量保证工作的一部分。由于难以找到能在自己\
负责的 Debian 任务上花费足够时间的开发人员，所以我们发现检查申请人是否能够长期坚持\
工作并把工作做好是很重要的事情。因此，我们要求准成员已经积极参与 Debian 一段时间。</p>

<p><a name="developer-priveleges"></a>每个 Debian 开发人员：</p>
<ul>
  <li>是 Debian 项目的成员；</li>
  <li>被允许对整个项目的问题进行投票；</li>
  <li>可以登录大多数使 Debian 保持运行的系统；</li>
  <li>具有<em>所有</em>软件包的上传权限（非负责打包的开发人员除外）；</li>
  <li>有权访问 debian-private 邮件列表。</li>
</ul>

<p>换句话说，成为 Debian 开发人员将为您提供多个有关项目基础架构的重要特权。很显然，\
这需要对申请人的极大信任以及申请人的遵守承诺。</p>

<p>因此，整个新成员过程非常严格和彻底。这并不是要劝阻有兴趣成为正式开发人员的人，\
但这确实可以解释为什么新成员过程需要这么多时间。</p>

<p>在阅读其余页面之前，请先查看<a href="#Glossary">词汇表定义</a>。</p>

<p>以下页面是申请人会感兴趣的：</p>

<ul>
 <li><a href="nm-checklist">清单————申请人所需进行的步骤</a>
  <ul>
   <li><a href="nm-step1">步骤 1: 申请</a></li>
   <li><a href="nm-step2">步骤 2: 身份验证</a></li>
   <li><a href="nm-step3">步骤 3: 哲学与工作程序</a></li>
   <li><a href="nm-step4">步骤 4: 任务和技能</a></li>
   <li><a href="nm-step5">步骤 5: 推荐</a></li>
   <li><a href="nm-step6">步骤 6: 前台检查</a></li>
   <li><a href="nm-step7">步骤 7: Debian 帐号管理员检查和创建帐号</a></li>
  </ul></li>
 <li><a href="https://nm.debian.org/public/newnm">报名表</a></li>
</ul>

<p>如果您已经是 Debian 开发人员，并且有兴趣参与新成员过程，请访问以下页面：</p>
<ul>
  <li><a href="nm-amchecklist">给申请管理员的检查清单</a></li>
  <li><a href="nm-advocate">推荐一个准成员</a></li>
  <li><a href="nm-amhowto">给申请管理员的迷你流程说明</a></li>
  <li><a href="$(HOME)/events/keysigning">关于密钥签名的迷你说明</a></li>
</ul>

<p>杂项：</p>
<ul>
  <li><a href="https://nm.debian.org/">新成员过程的状态数据</a></li>
  <li><a href="https://nm.debian.org/process/">当前的申请人名单</a></li>
  <li><a href="https://nm.debian.org/public/managers">当前的申请管理员名单</a></li>
</ul>

<define-tag email>&lt;<a href="mailto:%0">%0</a>&gt;</define-tag>

<h2><a name="Glossary">词汇表定义</a></h2>
<dl>
 <dt><a name="Advocate">推荐人</a>:</dt>
  <dd>一个推荐申请人的<a href="#Member">Debian 成员</a>。他们应该非常了解\
   <a href="#Applicant">申请人</a>，并且应该能够概述申请人所做的工作，兴趣和\
   计划。推荐人通常是申请人的<a href="#Sponsor">赞助者</a>。
  </dd>

 <dt><a name="Applicant">申请人</a>，新成员，历史上也被称为新维护者（NM）：</dt>
  <dd>一个请求成为 Debian 开发人员的人。</dd>

 <dt><a name="AppMan">申请管理员</a>（AM）：</dt>
  <dd>被指定为负责收集<a href="#DAM"> Debian 帐号管理员</a>所需的一个\
   <a href="#Applicant">申请人</a>的信息的一个<a href="#Member"> Debian 成员</a>，\
   收集的信息用于 Debian 帐号管理员对该申请的决定。同一个申请管理员可以被指定\
   负责不止一个申请人。</dd>

 <dt><a name="DAM"> Debian 帐号管理员</a>（DAM）： <email da-manager@debian.org></dt>
  <dd>一个被 Debian 项目负责人（DPL）任命负责管理 Debian 帐号创建和删除的\
   <a href="#Member"> Debian 成员</a>。DAM 拥有对申请程序的最终决定权。</dd>

 <dt><a name="FrontDesk">前台</a>: <email nm@debian.org></dt>
  <dd>前台成员为新成员过程完成基础结构性的工作，例如，接收初始申请、推荐消息\
   和最终的申请报告，以及将分配 AM 负责的 NM。如果申请程序出现问题，他们是联系点。</dd>

 <dt><a name="Member">成员，开发人员</a>：</dt>
  <dd>一个已通过新成员过程并接受其申请的 Debian 成员。</dd>

 <dt><a name="Sponsor">赞助者</a>：</dt>
  <dd>一个充当申请人的良师益友角色的<a href="#Member"> Debian 成员</a>：他们\
   检查由申请人提供的软件包，并帮助发现问题和改进打包。当赞助者对软件包感到满意\
   时，他们将替申请人将其上传到 Debian 档案库。尽管事实上申请人没有自己上传软件包，\
   但申请人仍会被记录为此软件包的维护者。</dd>
</dl>
