#use wml::debian::template title="如何参与"
#use wml::debian::translation-check translation="aedba88f0c7572ec26770e786608b2be7d5e3001" maintainer="Boyuan Yang"

<p>Debian 计划由志愿者所组成，我们的产品也完全由志愿者所开发而得到。\
我们一直<a href="$(HOME)/intro/diversity">欢迎新的贡献者</a>，只要您对自由软件\
有兴趣又有一定空闲时间便能参与进来。</p>

<p>在一切开始之前，您应当通读一遍网站上的各个页面来加深对我们工作的了解。\
请仔细阅读 <a href="$(HOME)/social_contract#guidelines">Debian 自由软件指导方针</a>\
以及我们的<a href="$(HOME)/social_contract">社群契约</a>。</p>

<p>计划内相当一部分沟通交流都使用<a href="$(HOME)/MailingLists/">邮件列表</a>。\
如果您想要对 Debian 计划内部的工作有一份直观的体验的话，您应当至少订阅
debian-devel-announce 和 debian-news 邮件列表。\
两者均为低容量的列表（并不会有大量邮件产生），但记录了社区内正在发生的事件。\
Debian 计划快讯（在 debian-news 上发布）总结了近期邮件列表和博客上出现的讨论\
内容并提供了指向原始内容的链接。\
作为一名潜在的开发人员，您应当考虑订阅 debian-mentors 列表；这里可以公开地\
进行讨论并为新加入的软件包维护者提供帮助（比较少见但也会出现的内容还有对\
那些想要帮助 Debian 但暂时不想参与打包工作的新人进行指导）。\
另外还有一些重要的邮件列表，例如 debian-devel、debian-project、debian-release、\
debian-qa 等等，您可以根据兴趣进行订阅。请参见\
<a href="$(HOME)/MailingLists/subscribe">邮件列表订阅</a>页面以了解完整的清单。\
（如果您想减少收到的邮件数量的话，可以考虑使用“-digest”的列表：这个功能可以\
帮助提供只读的邮件摘要，以方便订阅高容量列表的用户减轻负担。除此之外，\
您还可以使用<a href="https://lists.debian.org/">邮件列表存档</a>页面来使用\
网页浏览器阅读历史邮件。）</p>

<p><b>进行贡献</b>
如果您对维护软件包感兴趣的话，您应当关注一下我们的\
<a href="$(DEVEL)/wnpp/">需要投入精力的和未来的软件包</a>列表以查看哪些软件包\
需要维护者。接管被遗弃的软件包是作为软件包维护者开展工作的最佳途径——\
这样做不仅可以协助 Debian 提升软件包的维护质量，也是一次从先前的维护者身上\
进行学习的机会。</p>

<p>您也可以通过<a href="$(HOME)/doc">编写文档</a>的方式进行贡献；帮助\
<a href="$(HOME)/devel/website/">维护网站</a>、\
<a href="$(HOME)/international/">进行翻译</a>（国际化和本地化）、\
进行宣传、提供法律支持或在 Debian 社区中以其它角色参与活动。\
我们的<a href="https://qa.debian.org/">质保</a>网站也列出了其它的一些选项。
</p>

<p>您不需要成为正式的 Debian 开发者便可以为这些计划任务做出贡献。\
现有的 Debian 开发者可以以<a href="newmaint#Sponsor">赞助者</a>的身份帮助\
将您的工作成果整合到整个计划中去。通常比较推荐您先找到一名与您的兴趣方向\
相近的开发者并互相协助，以方便开展工作。</p>

<p>最后，Debian 提供了很多开发人员 <a
href="https://wiki.debian.org/Teams">团队</a> 来共同完成常见任务。无论是否为正式的\
 Debian 开发人员，任何人都可以参加团队。与团队合作是在开始<a
href="newmaint">新成员流程</a>之前获得经验的一种很好的方法，并且是寻找赞助者的\
最佳场所之一。因此，找到适合您兴趣的团队并立即加入。</p>

<p>
<b>加入</b>
在您贡献了一段时间并确定参与了 Debian 项目之后，您可以以更正式的角色加入 Debian。\
这里有两种不同的角色供您选择加入 Debian：
</p>

<ul>
<li>Debian 维护者（DM）：成为 DM 后，您可以将自己打包的软件包上传到 Debian 软件档案库。</li>
<li>Debian 开发人员（DD）：Debian 中的传统正式成员。 DD 能参与 Debian 选举。\
负责打包的 DD 能将任何软件包上传到软件档案库。在申请成为负责打包的 DD 之前，\
您应有至少六个月的打包维护记录。例如：作为 DM 上传软件包，在团队内部工作或维护\
由赞助者上传的软件包。非负责打包的 DD 在软件档案库中没有权限。在申请成为非负责打包\
的 DD 之前，您应该在项目中具有可见且重要的工作记录。</li>

</ul>

<p>尽管 DM 和 DD 的许多权利和责任是相同的，但是目前有相互独立的流程来申请这两种角色。\
有关成为 Debian 维护者的详细信息，请参见<a 
href="https://wiki.debian.org/DebianMaintainer">Debian 维护者 Wiki 页面</a>。
也请查看<a
href="newmaint">新成员专区</a> 页面了解如何申请正式 Debian 开发人员身份。</p>

<p>请注意，在 Debian 的大部分历史中，Debian 开发人员角色是唯一的角色。\
Debian 维护者角色是在 2007 年 8 月 5 日引入的。这就是为什么您会发现“ 维护者”（maintainer）\
一词在许多历史记录中被使用，而术语“Debian 开发人员”（Debian Developer）用于那些地方\
会更准确。例如，直到 2011 年，申请成为 Debian 开发人员的过程仍被称为“ Debian 新维护者”\
（Debian New Maintainer）过程，当时该过程被重命名为“Debian 新成员”\
（Debian New Member）过程。</p>

<p>无论选择申请哪种角色，都应该熟悉 Debian 的程序，因此建议在申请前阅读<a
href="$(DOC)/debian-policy/">Debian 政策</a> 和 <a
href="$(DOC)/developers-reference/">开发人员参考</a>。</p>

<p>除了成为开发人员，您还可以在很多方面<a href="$(HOME)/intro/help">为 Debian 提供帮助\
</a>，包括测试、编写文档、移植、<a href="$(HOME)/donations">捐赠</a>金钱与机器以帮助\
开发和改善连接可用性。我们一直在世界各处寻求<a href="$(HOME)/mirror/">镜像站点</a>。</p>
