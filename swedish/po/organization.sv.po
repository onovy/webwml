# translation of organization.po to Swedish
#
# Martin Ågren <martin.agren@gmail.com>, 2010.
msgid ""
msgstr ""
"Project-Id-Version: organization\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: (null)\n"
"PO-Revision-Date: 2019-11-16 01:04+0100\n"
"Last-Translator: Andreas Rönnquist <andreas@ronnquist.net>\n"
"Language-Team: Swedish <debian-l10n-swedish@lists.debian.org>\n"
"Language: sv\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 2.2.1\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#: ../../english/intro/organization.data:15
msgid "delegation mail"
msgstr "delegeringsmail"

#: ../../english/intro/organization.data:16
msgid "appointment mail"
msgstr "utnämningsmeddelande"

#. One male delegate
#: ../../english/intro/organization.data:18
msgid "<void id=\"male\"/>delegate"
msgstr "<void id=\"male\"/>delegat"

#. One female delegate
#: ../../english/intro/organization.data:20
msgid "<void id=\"female\"/>delegate"
msgstr "<void id=\"female\"/>delegat"

#: ../../english/intro/organization.data:23
#: ../../english/intro/organization.data:25
msgid "current"
msgstr "nuvarande"

#: ../../english/intro/organization.data:27
#: ../../english/intro/organization.data:29
msgid "member"
msgstr "medlem"

#: ../../english/intro/organization.data:32
msgid "manager"
msgstr "chef"

#: ../../english/intro/organization.data:34
msgid "Stable Release Manager"
msgstr "Samordning av nya stabila utgåvor"

#: ../../english/intro/organization.data:34
msgid "SRM"
msgstr "SRM"

#: ../../english/intro/organization.data:36
msgid "wizard"
msgstr "mästare"

#. we only use the chair tag once, for techctte, I wonder why it's here.
#: ../../english/intro/organization.data:38
msgid "chair"
msgstr "ordförande"

#: ../../english/intro/organization.data:41
msgid "assistant"
msgstr "assistent"

#: ../../english/intro/organization.data:43
msgid "secretary"
msgstr "sekreterare"

#: ../../english/intro/organization.data:45
msgid "representative"
msgstr "representant"

#: ../../english/intro/organization.data:47
msgid "role"
msgstr "toll"

#: ../../english/intro/organization.data:63
#: ../../english/intro/organization.data:75
msgid "Officers"
msgstr "Tjänstemän"

#: ../../english/intro/organization.data:64
#: ../../english/intro/organization.data:99
msgid "Distribution"
msgstr "Distributionen"

#: ../../english/intro/organization.data:65
#: ../../english/intro/organization.data:235
msgid "Communication and Outreach"
msgstr "Kommunikation och Utåtriktad verksamhet"

#: ../../english/intro/organization.data:67
#: ../../english/intro/organization.data:238
msgid "Data Protection team"
msgstr "Dataskyddsgruppen"

#: ../../english/intro/organization.data:68
#: ../../english/intro/organization.data:242
msgid "Publicity team"
msgstr "Publicitetsgruppen"

#: ../../english/intro/organization.data:70
#: ../../english/intro/organization.data:311
msgid "Membership in other organizations"
msgstr "Medlemskap i andra organisationer"

#: ../../english/intro/organization.data:71
#: ../../english/intro/organization.data:339
msgid "Support and Infrastructure"
msgstr "Support och infrastruktur"

#: ../../english/intro/organization.data:78
msgid "Leader"
msgstr "Arbetsledare"

#: ../../english/intro/organization.data:80
msgid "Technical Committee"
msgstr "Teknisk kommitté"

#: ../../english/intro/organization.data:94
msgid "Secretary"
msgstr "Sekreterare"

#: ../../english/intro/organization.data:102
msgid "Development Projects"
msgstr "Utvecklingsprojekt"

#: ../../english/intro/organization.data:103
msgid "FTP Archives"
msgstr "Ftp-arkiv"

#: ../../english/intro/organization.data:105
msgid "FTP Masters"
msgstr "FTP Masters"

#: ../../english/intro/organization.data:111
msgid "FTP Assistants"
msgstr "Ftp-assistenter"

#: ../../english/intro/organization.data:116
msgid "FTP Wizards"
msgstr "FTP Wizards"

#: ../../english/intro/organization.data:120
msgid "Backports"
msgstr "Backports"

#: ../../english/intro/organization.data:122
msgid "Backports Team"
msgstr "Backports-teamet"

#: ../../english/intro/organization.data:126
msgid "Release Management"
msgstr "Samordning av nya utgåvor"

#: ../../english/intro/organization.data:128
msgid "Release Team"
msgstr "Utgivningsgruppen"

#: ../../english/intro/organization.data:141
msgid "Quality Assurance"
msgstr "Kvalitetsstyrning"

#: ../../english/intro/organization.data:142
msgid "Installation System Team"
msgstr "Installationssystemgruppen"

#: ../../english/intro/organization.data:143
msgid "Debian Live Team"
msgstr "Debian Live-gruppen"

#: ../../english/intro/organization.data:144
msgid "Release Notes"
msgstr "Versionsfakta"

#: ../../english/intro/organization.data:146
msgid "CD Images"
msgstr "Cd-avbildningsfiler"

#: ../../english/intro/organization.data:148
msgid "Production"
msgstr "Produktion"

#: ../../english/intro/organization.data:156
msgid "Testing"
msgstr "Testning"

#: ../../english/intro/organization.data:158
msgid "Cloud Team"
msgstr "Molngruppen"

#: ../../english/intro/organization.data:162
msgid "Autobuilding infrastructure"
msgstr "Infrastruktur för automatiska byggen"

#: ../../english/intro/organization.data:164
msgid "Wanna-build team"
msgstr "Vill-bygga-gruppen"

#: ../../english/intro/organization.data:171
msgid "Buildd administration"
msgstr "Byggserveradministration"

#: ../../english/intro/organization.data:189
msgid "Documentation"
msgstr "Dokumentation"

#: ../../english/intro/organization.data:194
msgid "Work-Needing and Prospective Packages list"
msgstr "Paket med behov av arbete samt framtida paket"

#: ../../english/intro/organization.data:196
msgid "Ports"
msgstr "Anpassningar"

#: ../../english/intro/organization.data:226
msgid "Special Configurations"
msgstr "Speciella system"

#: ../../english/intro/organization.data:228
msgid "Laptops"
msgstr "Bärbara"

#: ../../english/intro/organization.data:229
msgid "Firewalls"
msgstr "Brandväggar"

#: ../../english/intro/organization.data:230
msgid "Embedded systems"
msgstr "Inbyggda system"

#: ../../english/intro/organization.data:245
msgid "Press Contact"
msgstr "Presskontakt"

#: ../../english/intro/organization.data:247
msgid "Web Pages"
msgstr "Webbsidor"

#: ../../english/intro/organization.data:259
msgid "Planet Debian"
msgstr "Planet Debian"

#: ../../english/intro/organization.data:264
msgid "Outreach"
msgstr "Outreach"

#: ../../english/intro/organization.data:269
msgid "Debian Women Project"
msgstr "Debian Women-projektet"

#: ../../english/intro/organization.data:277
msgid "Anti-harassment"
msgstr "Anti-trakasserier"

#: ../../english/intro/organization.data:282
msgid "Events"
msgstr "Evenemang"

#: ../../english/intro/organization.data:289
msgid "DebConf Committee"
msgstr "DebConf-kommitté"

#: ../../english/intro/organization.data:296
msgid "Partner Program"
msgstr "Partnerprogrammet"

#: ../../english/intro/organization.data:301
msgid "Hardware Donations Coordination"
msgstr "Samordning av maskinvarudonationer"

#: ../../english/intro/organization.data:317
msgid "GNOME Foundation"
msgstr "GNOME Foundation"

#: ../../english/intro/organization.data:319
msgid "Linux Professional Institute"
msgstr "Linux Professional Institute"

#: ../../english/intro/organization.data:321
msgid "Linux Magazine"
msgstr "Linux Magazine"

#: ../../english/intro/organization.data:323
msgid "Linux Standards Base"
msgstr "Linux Standards Base"

#: ../../english/intro/organization.data:325
msgid "Free Standards Group"
msgstr "Free Standards Group"

#: ../../english/intro/organization.data:326
msgid "SchoolForge"
msgstr "Schoolforge"

#: ../../english/intro/organization.data:329
msgid ""
"OASIS: Organization\n"
"      for the Advancement of Structured Information Standards"
msgstr ""
"OASIS: Organization\n"
"      for the Advancement of Structured Information Standards"

#: ../../english/intro/organization.data:332
msgid ""
"OVAL: Open Vulnerability\n"
"      Assessment Language"
msgstr ""
"OVAL: Open Vulnerability\n"
"      Assessment Language"

#: ../../english/intro/organization.data:335
msgid "Open Source Initiative"
msgstr "Open Source Initiative"

#: ../../english/intro/organization.data:342
msgid "User support"
msgstr "Användarstöd"

#: ../../english/intro/organization.data:409
msgid "Bug Tracking System"
msgstr "Felrapporteringssystem"

#: ../../english/intro/organization.data:414
msgid "Mailing Lists Administration and Mailing List Archives"
msgstr "Sändlisteadministration och -arkiv"

#: ../../english/intro/organization.data:422
msgid "New Members Front Desk"
msgstr "Nymedlemsreceptionen"

#: ../../english/intro/organization.data:428
msgid "Debian Account Managers"
msgstr "Debiankontoadministratörer"

#: ../../english/intro/organization.data:432
msgid ""
"To send a private message to all DAMs, use the GPG key "
"57731224A9762EA155AB2A530CA8D15BB24D96F2."
msgstr ""
"För att skicka ett privatmeddelande till alla DAMs, använd GPG-nyckeln "
"57731224A9762EA155AB2A530CA8D15BB24D96F2."

#: ../../english/intro/organization.data:433
msgid "Keyring Maintainers (PGP and GPG)"
msgstr "Ansvariga för nyckelring (PGP och GPG)"

#: ../../english/intro/organization.data:437
msgid "Security Team"
msgstr "Säkerhetsgrupp"

#: ../../english/intro/organization.data:448
msgid "Consultants Page"
msgstr "Konsultsida"

#: ../../english/intro/organization.data:453
msgid "CD Vendors Page"
msgstr "Cd-försäljarsida"

#: ../../english/intro/organization.data:456
msgid "Policy"
msgstr "Policy"

#: ../../english/intro/organization.data:459
msgid "System Administration"
msgstr "Systemadministration"

#: ../../english/intro/organization.data:460
msgid ""
"This is the address to use when encountering problems on one of Debian's "
"machines, including password problems or you need a package installed."
msgstr ""
"Det här är adressen som skall användas när problem uppstår med en av Debians "
"datorer, inklusive lösenordsproblem, eller om du behöver få ett paket "
"installerat."

#: ../../english/intro/organization.data:469
msgid ""
"If you have hardware problems with Debian machines, please see <a href="
"\"https://db.debian.org/machines.cgi\">Debian Machines</a> page, it should "
"contain per-machine administrator information."
msgstr ""
"Om du har maskinvaruproblem med en av Debians datorer, se <a href=\"https://"
"db.debian.org/machines.cgi\">Debianmaskinsidan</a>, vilken innehåller "
"information om administratörer för varje enskild maskin."

#: ../../english/intro/organization.data:470
msgid "LDAP Developer Directory Administrator"
msgstr "Administratör för LDAP-utvecklarkatalogen"

#: ../../english/intro/organization.data:471
msgid "Mirrors"
msgstr "Speglingar"

#: ../../english/intro/organization.data:478
msgid "DNS Maintainer"
msgstr "DNS-ansvarig"

#: ../../english/intro/organization.data:479
msgid "Package Tracking System"
msgstr "Paketspårningssystem"

#: ../../english/intro/organization.data:481
msgid "Treasurer"
msgstr "Kassör"

#: ../../english/intro/organization.data:488
msgid ""
"<a name=\"trademark\" href=\"m4_HOME/trademark\">Trademark</a> use requests"
msgstr ""
"Förfrågningar angående <a href=\"m4_HOME/trademark\">varumärkesanvändning</a>"

#: ../../english/intro/organization.data:491
msgid "Salsa administrators"
msgstr "Salsaadministratörer"

#~ msgid "Debian Pure Blends"
#~ msgstr "Debian Pure Blends"

#~ msgid "Debian for children from 1 to 99"
#~ msgstr "Debian för barn mellan 1 och 99"

#~ msgid "Debian for medical practice and research"
#~ msgstr "Debian för medicinsk praktik och forskning"

#~ msgid "Debian for education"
#~ msgstr "Debian för utbildning"

#~ msgid "Debian in legal offices"
#~ msgstr "Debian för juridiska firmor"

# Politiskt korrekt? :-)
#~ msgid "Debian for people with disabilities"
#~ msgstr "Debian för personer med funktionsnedsättningar"

#~ msgid "Debian for science and related research"
#~ msgstr "Debian för vetenskap och forskning"

#~ msgid "Debian for astronomy"
#~ msgstr "Debian för astronomi"

#~ msgid "Alioth administrators"
#~ msgstr "Administratörer för Alioth"

#~ msgid "Bits from Debian"
#~ msgstr "Bitar från Debian"

#~ msgid "DebConf chairs"
#~ msgstr "Debconf-ledare"

#~ msgid "Testing Security Team"
#~ msgstr "Uttestningsutgåvans säkerhetsgrupp"

#~ msgid "Security Audit Project"
#~ msgstr "Projekt för säkerhetsgenomgång"

#~ msgid "Key Signing Coordination"
#~ msgstr "Samordning av nyckelsignering"

#~ msgid "Marketing Team"
#~ msgstr "Marknadsföringsgrupp"

#~ msgid "Handhelds"
#~ msgstr "Handhållna"

#~ msgid "Vendors"
#~ msgstr "Försäljare"

#~ msgid "Volatile Team"
#~ msgstr "Volatilegruppen"

#~ msgid "current Debian Project Leader"
#~ msgstr "Debians nuvarande projektledare"

#~ msgid "Debian Maintainer (DM) Keyring Maintainers"
#~ msgstr "Ansvariga för nyckelringen för Debian Maintainers (DM)"

#~ msgid "Publicity"
#~ msgstr "PR"

#~ msgid "Auditor"
#~ msgstr "Revisor"

#~ msgid "Live System Team"
#~ msgstr "Livesystem-gruppen"

#~ msgid "Individual Packages"
#~ msgstr "Individuella paket"
